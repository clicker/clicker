<?php
/* Tiny clicker -- a clicker web application to support peer instruction
   Copyright (C) 2012 Marcel Kyas <marcel.kyas@fu-berlin.de>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

if (!isset($_GET['poll']))
    die();

$poll = $_GET['poll'];
require('controller.php');
$controller = new Controller();
$question = $controller->question($poll);
$answers = $controller->open($poll);
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Poll is open</title>
    <link rel="stylesheet" type="text/css" href="clicker.css" />
    <script type="text/javascript" src="clicker.js"></script>
  </head>
  <body onload="startCountdown('<?php echo $poll; ?>')">
    <h1><?php echo $question; ?></h1>
    <ol>
<?php
foreach($answers as $answer) {
    echo "    <li>" . $answer['answer'] . "</li>\n";
}
?>
    </ol>
    <div id="count"></div>
<?php $controller->footer(); ?>
  </body>
</html>
