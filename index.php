<?php
/* Tiny clicker -- a clicker web application to support peer instruction
   Copyright (C) 2012 Marcel Kyas <marcel.kyas@fu-berlin.de>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

require('controller.php');
$controller = new Controller();
$poll = $controller->open_poll();
$question = $controller->question($poll);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title><?php if ($question) echo $question; else echo "Tiny Poll."; ?></title>
    <link rel="stylesheet" type="text/css" href="clicker.css" />
    <script type="text/javascript" src="clicker.js"></script>
  </head>
  <body>
<?php
if (!$poll) {
    echo "    <h1>No poll is open at this time</h1>\n";
    $controller->footer();
    echo "  </body>\n</html>\n";
    exit;
}
?>
    <h1>Cast your vote</h1>
    <p><?php echo $question; ?></p>
    <div id="clicker">
      <form action="clicker.php" method="get">
        <fieldset>
<?php
$answers = $controller->answers($poll);
foreach($answers as $answer) {
    echo '          <input type="radio" name="clicker" required="true" value="' . $answer['choice'] . '" onclick="vote(\'' . $poll . '\', this.value)" /> ' . $answer['answer'] . '<br />
';
}
?>
        </fieldset>
        <noscript>
          <input type="hidden" name="poll" value="<?php echo $poll; ?>" />
          <input type="submit" value="Submit" />
        </noscript>
      </form>
    </div>
<?php $controller->footer(); ?>
  </body>
</html>
