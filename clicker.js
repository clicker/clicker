/* Tiny clicker -- a clicker web application to support peer instruction
   Copyright (C) 2012 Marcel Kyas <marcel.kyas@fu-berlin.de>
  
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
  
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.
  
   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

var timer = null;
var count = 30;
function countdown(poll) {
  var out = document.getElementById("count");
  if (count > 0)
    out.innerHTML = "Poll closes in " + count.toString() + " seconds";
  else
    out.innerHTML = "Poll is closed";
  count--;
  if (count < 0) {
    window.clearInterval(timer);
    window.location.href = "close.php?poll=" + poll;
  }
}

function startCountdown(poll) {
  timer = window.setInterval("countdown('" + poll + "')", 1000);
  countdown(poll);
}

function vote(poll, value) {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange =
    function () {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        document.getElementById("clicker").innerHTML=xmlhttp.responseText;
      }
    };
  xmlhttp.open("GET", "clicker.php?poll=" + poll +"&click=" + value, true);
  xmlhttp.send(null);
}

function makeGraph() {
  var container = document.getElementById("graph");
  var labels = document.getElementById("labels");
  var dnl = container.getElementsByTagName("li");
  for(var i = 0; i < dnl.length; i++) {
    var item = dnl.item(i);
    var content = item.innerHTML.split(";");
    var value = content[0];
    var color = item.style.background;
    item.style.left = (i * 80 + 20) + "px";
    item.style.top = (199 - 2 * value) + "px";
    item.style.height = (value * 2) + "px";
    item.innerHTML = value;
    item.style.visibility="visible";
    left = new String(i * 68 + 102) + "px";
    labels.innerHTML = labels.innerHTML +
       "<span style='position:relative;top:-4px;left:"+ 
       left+";background:"+ color+"'>" + content[1] + "</span>";
  }   
}
