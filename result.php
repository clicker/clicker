<?php
/* Tiny clicker -- a clicker web application to support peer instruction
   Copyright (C) 2012 Marcel Kyas <marcel.kyas@fu-berlin.de>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

require('controller.php');
$poll = $_GET['poll'];
$date = $_GET['date'];
$controller = new Controller();
$question = $controller->question($poll);
?>
<!DOCTYPE html>
<html>
  <head>
    <title><?php echo $question; ?> (Results)</title>
    <link rel="stylesheet" type="text/css" href="clicker.css" />
    <script type="text/javascript" src="clicker.js"></script>
  </head>
  
<?php
$result = $controller->open_poll($poll);
if ($result === $poll) {
    echo '  <body>
    <h1>Poll ' . $poll . ' is still open</h1>
  </body>
</html>
';
    exit;
}
?>
  <body onload="makeGraph()";>
    <h1><?php echo $question; ?> (Results)</h1>
    <div id="graph">
      <div style="position: absolute; top: 6px; left: 0">100</div>
      <div style="position: absolute; top: 46px; left: 0">80</div>
      <div style="position: absolute; top: 86px; left: 0">60</div>
      <div style="position: absolute; top: 126px; left: 0">40</div>
      <div style="position: absolute; top: 166px; left: 0">20</div>
      <div style="position: absolute; top: 206px; left: 0">0</div>
      <ul>
<?php
$result = $controller->result($poll, $date);
$letter = array( 'A', 'B', 'C', 'D', 'E', 'F');
foreach ($result as $column) {
  echo "        <li>" . $column['votes'] . ";" . $letter[$column['choice']] . "</li>\n";
}
?>
      </ul>
    </div>
    <div id="labels"></div>
    <p><?php echo $result[0]['sum']; ?> votes.</p>
<?php $controller->footer(); ?>
  </body>
</html>
