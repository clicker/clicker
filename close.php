<?php
/* Tiny clicker -- a clicker web application to support peer instruction
   Copyright (C) 2012 Marcel Kyas <marcel.kyas@fu-berlin.de>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

require('controller.php');
$poll = $_GET['poll'];
$controller = new Controller();
$result = $controller->close($poll);
?>

<!DOCTYPE html>
<html>
  <head>
    <title>Poll is closed</title>
    <link rel="stylesheet" type="text/css" href="clicker.css" />
  </head>
  <body>
    <p>Poll is now closed.</p>
    <p><a href="result.php?poll=<?php echo $result['poll']?>&date=<?php echo urlencode($result['date']); ?>">Result</a></p>
<?php $controller->footer(); ?>
  </body>
</html>
