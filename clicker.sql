-- Tiny clicker -- a clicker web application to support peer instruction
-- Copyright (C) 2012 Marcel Kyas <marcel.kyas@fu-berlin.de>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-- Data declarations for PostgreSQL. Should work for most other ANSI
-- SQL data bases.

CREATE TABLE poll
  (poll CHARACTER VARYING(42) PRIMARY KEY CHECK (length(poll) > 0),
   title CHARACTER VARYING(80) NOT NULL CHECK (length(title) > 0),
   time INTEGER NOT NULL CHECK (time > 0));

CREATE TABLE answers
  (poll CHARACTER VARYING(42) NOT NULL REFERENCES poll(poll) ON DELETE CASCADE,
   choice INTEGER NOT NULL CHECK (0 <= choice AND choice < 6),
   answer TEXT NOT NULL CHECK (length(answer) > 0),
   PRIMARY KEY (poll, choice));

CREATE TABLE open_polls
  (poll CHARACTER VARYING(42) PRIMARY KEY REFERENCES poll ON DELETE CASCADE);

CREATE TABLE votes
  (poll CHARACTER VARYING(42) NOT NULL REFERENCES poll ON DELETE CASCADE,
   choice INTEGER NOT NULL CHECK (0 <= choice AND choice < 6),
   votes INTEGER NOT NULL CHECK (votes >= 0),
   PRIMARY KEY (poll, choice),
   FOREIGN KEY (poll, choice) REFERENCES answers);

CREATE TABLE voters
  (poll CHARACTER VARYING(42) REFERENCES open_polls ON DELETE CASCADE,
   ip inet NOT NULL,
   PRIMARY KEY (poll, ip));

CREATE TABLE log
  (date TIMESTAMP WITH TIME ZONE NOT NULL,
   poll CHARACTER VARYING(42) NOT NULL REFERENCES poll(poll) ON DELETE CASCADE,
   choice INTEGER NOT NULL CHECK (0 <= choice AND choice < 6),
   votes INTEGER NOT NULL CHECK (votes >= 0),
   PRIMARY KEY (date, poll, choice),
   FOREIGN KEY (poll, choice) REFERENCES answers);
