<?php
/* Tiny clicker -- a clicker web application to support peer instruction
   Copyright (C) 2012 Marcel Kyas <marcel.kyas@fu-berlin.de>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

require('config.php');
error_reporting(E_ALL);

class Controller {
    private $dbh;

    function __construct($setup = false) {
        global $database;
        try {
            $this->dbh = new PDO($database);
        } catch (Exception $e) {
            die("Failed to connect to data base: " . $e->getMessage());
        }
	$this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /* Add a poll with answers to the data base.
     *
     * [poll] uniquely identifies the poll. Must not be null or the
     *   empty string.
     * [question] is a text that represents the poll question.
     * [answers] is an array of four possible answers, numberd from 0 to 3.
     */
    public function add_question(&$poll, &$question, &$answer) {
	try {
            $result = $this->dbh->beginTransaction();
            $sql = "INSERT INTO poll VALUES (:poll, :question)";
	    $stmt = $this->dbh->prepare($sql);
	    $result = $stmt->execute(array(':poll' => $poll,
                                           ':question' => $question));
            $sql = "INSERT INTO answers VALUES (:poll, :choice, :answer)";
	    $stmt = $this->dbh->prepare($sql);
            for (array_keys($answer) as $i)
                $result = $stmt->execute(array(':poll' => $poll,
                                               ':choice' => $i,
                                               ':answer' => $answer[$i]));
            $result = $this->dbh->commit();
        } catch (Exception $e) {
            $this->dbh->rollBack();
        }
    }

    /* Open a new poll and return all its answers. */
    public function open(&$poll) {
	// Fail if there is a poll currently running.
	$tp = $this->open_poll();
	if (isset($tp))
           return;

        // Get the answers to the poll, we need these to initialise the
	// votes table.
	$answers = $this->answers($poll);

        // Initialise the newly opend poll
	$args = array(':poll' => $poll);
        try {
            $this->dbh->beginTransaction();
            $stmt = $this->dbh->prepare("DELETE FROM voters WHERE poll = :poll");
            $result = $stmt->execute($args);

            $stmt = $this->dbh->prepare('DELETE FROM votes WHERE poll = :poll');
	    $stmt->execute($args);
            $stmt = $this->dbh->prepare("INSERT INTO votes VALUES (:poll, :choice, 0)");
            foreach ($answers as $answer) {
                $args[':choice'] = $answer['choice'];
	        $stmt->execute($args);
            }
            unset($args[':choice']);
            $stmt = $this->dbh->prepare("INSERT INTO open_polls VALUES (:poll)");
	    $stmt->execute($args);
            $result = $this->dbh->commit();
        } catch (Exception $e) {
            $this->dbh->rollBack();
	    die('Open failed :' . $e->getMessage());
        }
	return $answers;
    }

    public function open_poll() {
        $result = $this->dbh->query("SELECT poll FROM open_polls");
        $poll = $result->fetchColumn();
        return $poll;
    }

    public function is_poll_open(&$poll) {
	if (!isset($poll) || $poll == "")
            return false;
        $result = $this->open_poll();
        return ($poll == $result);
    }

    public function close(&$poll) {
        // Get the votes and prepare for logging.
        $sql = "SELECT choice, votes FROM votes WHERE poll = :poll " .
               "ORDER BY choice ASC";
	$stmt = $this->dbh->prepare($sql);
        $result = $stmt->execute(array(':poll' => $poll));
        if (!$result)
            die("Failed to query data base.");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	// Insert them into the log table.
        $args = array(':poll' => $poll,
                      ':date' => gmdate('Y-m-d H:i:sO', time()));
        $sql = "INSERT INTO log VALUES (:date, :poll, :choice, :votes)";
        $stmt = $this->dbh->prepare($sql);
	try {
	    $this->dbh->beginTransaction();
	    foreach ($results as $row) {
	        $args['choice'] = $row['choice'];
	        $args['votes'] = $row['votes'];
	        $stmt->execute($args);
	    }
	    $this->dbh->commit();
	} catch (Exception $e) {
	    $this->dbh->rollBack();
	    die("Inserting into log failed: " . $e->getMessage());
	}

        // Forget about all IPs that cast a vote.
	try {
	    $this->dbh->beginTransaction();

	    /* Disabled, see below.
	    // Remove all IPs that cast a vote for this poll.
	    $sql = "DELETE FROM voters WHERE poll = :poll";
            $stmt = $this->dbh->prepare($sql);
	    $stmt->bindParam(':poll', $poll);
            $result = $stmt->execute();
	    */

            // Close the poll such that no one can enter a new vote. This
            // should also remove all IPs that cast a vote in this poll.
	    $sql = "DELETE FROM open_polls WHERE poll = :poll";
            $stmt = $this->dbh->prepare($sql);
	    $stmt->bindParam(':poll', $poll);
            $result = $stmt->execute();
	    $this->dbh->commit();
	} catch (Exception $e) {
	    $this->dbh->rollBack();
	    die("Closing the poll failed: " . $e->getMessage());
        }

	// Now drop the votes
	$stmt = $this->dbh->prepare("DELETE FROM votes WHERE poll = :poll");
	$stmt->execute(array(':poll' => $poll));

        return array('date' => $args[':date'], 'poll' => $args[':poll']);
    }

    public function question(&$poll) {
	$sql = "SELECT title FROM poll WHERE poll = :poll";
	$stmt = $this->dbh->prepare($sql);
	$stmt->execute(array(':poll' => $poll));
	$result = $stmt->fetch(PDO::FETCH_ASSOC);
	$stmt->closeCursor();
	return $result['title'];
    }

    public function answers(&$poll) {
        $sql = "SELECT choice, answer FROM answers WHERE poll = :poll ORDER BY choice";
	$stmt = $this->dbh->prepare($sql);
        $stmt->execute(array(':poll' => $poll));
	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
	$stmt->closeCursor();
        return $result;
    }

    public function vote(&$poll, &$choice) {
        // Sanitize vote, which is supposed to be an integer.
	if ($this->open_poll($poll) != $poll) {
            return 'This poll is closed.';
	}
	// Did the user vote already?
	$ip = $_SERVER['REMOTE_ADDR'];
	$args = array(':poll' => $poll, ':ip' => $ip);
	$stmt = $this->dbh->prepare("SELECT ip FROM voters WHERE poll = :poll AND ip = :ip");
	$stmt->execute($args);
	$result = $stmt->fetch(PDO::FETCH_ASSOC);
	if (isset($result["ip"]))
            return 'A vote has already been cast from IP ' . $ip;

	// Prepare to cast the vote.
        try {
	    $this->dbh->beginTransaction();
	    $sql = "UPDATE votes SET votes = votes + 1 WHERE poll = :poll AND choice = :choice";
	    $stmt = $this->dbh->prepare($sql);
            $stmt->execute(array(':poll' => $poll, ':choice' => $choice));
	    $stmt = $this->dbh->prepare("INSERT INTO voters VALUES (:poll, :ip)");
	    $result = $stmt->execute($args);
	    $this->dbh->commit();
        } catch (Exception $e) {
	    $this->dbh->rollBack();
	    die("Voting failed " . $e->getMessage());
        }
	if ($result)
            return 'Thank you for your vote.';
        else
            return 'What?';
    }

    public function result(&$poll, &$date) {
        $sql = "SELECT choice, votes FROM log WHERE poll = :poll AND date = :date GROUP BY choice, votes ORDER BY choice";
        $stmt = $this->dbh->prepare($sql);
	$stmt->execute(array(':poll' => $poll, ':date' => $date));
	$qr = $stmt->fetchAll(PDO::FETCH_ASSOC);
	$result = array();
	$sum = 0;
	foreach ($qr as $row)
	    $sum += $row['votes'];
	foreach ($qr as $row)
	    $result[$row['choice']] = array('choice' => $row['choice'],
					   'votes' => ($sum != 0) ? 100 * $row['votes'] / $sum : 0,
					   'sum' => $sum);
        return $result;
    }


    /* Generate a footer with license information and the relevant
     * URI.
     */
    public function footer() {
      global $giturl;
      global $gittag;

      echo '    <div id="footer">
      <div id="git">Tiny <a href="' . $giturl . '">Clicker</a>, deployed version ' . $gittag . '.</div>
      <div id="license">Released as free software without warranties under <a href="http://www.gnu.org/licenses/agpl-3.0.html">GNU Affero GPL v3</a>.</div>
    </div>';
    }
}
