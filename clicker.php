<?php
/* Tiny clicker -- a clicker web application to support peer instruction
   Copyright (C) 2012 Marcel Kyas <marcel.kyas@fu-berlin.de>
,
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

if (!(isset($_GET['click']) && isset($_GET['poll'])))
    die();

require('controller.php');

$click = $_GET['click'];
$poll = $_GET['poll'];
$controller = new Controller();
$result = $controller->vote($poll, $click);
echo "<p>" . $result . "</p>\n";
?>
